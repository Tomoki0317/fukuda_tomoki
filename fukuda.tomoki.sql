CREATE DATABASE fukuda_tomoki;
DROP TABLE USERS;
CREATE TABLE USERS (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
    login_id VARCHAR(20) UNIQUE NOT NULL,
	password VARCHAR(255) NOT NULL,	
    name VARCHAR(10),
    branch_id INTEGER NOT NULL,
	position_id INTEGER NOT NULL,
    created_date TIMESTAMP NOT NULL,
	state tinyint(1) NOT NULL
);
CREATE TABLE BRANCH (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	branch VARCHAR(10) NOT NULL
);
CREATE TABLE POSITION (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	position VARCHAR(10) NOT NULL
);
CREATE TABLE POST (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    login_id VARCHAR(20) UNIQUE NOT NULL,
    title VARCHAR(30) NOT NULL,
    mainpost VARCHAR(1000) NOT NULL,
    category VARCHAR(10) NOT NULL,
    created_date TIMESTAMP NOT NULL
);
CREATE TABLE COMMENTPOST (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	login_id VARCHAR(20) UNIQUE NOT NULL,
	post_id INTEGER NOT NULL,
	maincomment VARCHAR(500) NOT NULL,
    created_date TIMESTAMP NOT NULL
);
